autoload -Uz compinit
fpath+=~/.zfunc
fpath+="$HOME/.nix-profile/share/zsh/site-functions"
if type brew &>/dev/null; then
	fpath+="/opt/homebrew/share/zsh/site-functions"
fi
compinit -u # obsidian f17c78

# longer history
HISTFILE=$HOME/.zsh_history
HISTSIZE=900000
SAVEHIST=900000
export SAVEHIST=$HISTSIZE
setopt hist_ignore_all_dups
setopt hist_ignore_space

# do not beep
setopt nobeep

# random features
setopt autocd


#custopm rompt
autoload -U colors && colors
PS1="%{$fg[red]%}%n%{$reset_color%}@%{$fg[red]%}%m%{$fg[cyan]%}:[%8c]:%{$reset_color%} "

# Do menu-driven completion.
zstyle ':completion:*' menu select

# use ZLE's vi-mode, but keep some standard behavior
#   https://dougblack.io/words/zsh-vi-mode.html
#   http://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html#Keymaps
#   also helpful: bindkey -l
#                 bindkey -M keymapname -> use this to find which actions
#                                          are available
bindkey -v
bindkey '^r' history-incremental-search-backward
function _cs-backward-kill-partword () {
  # do global replacement with nothing
  local WORDCHARS="${WORDCHARS//[-\/=]}"
  zle backward-delete-word
}
zle -N _cs-backward-kill-partword
bindkey '^w' _cs-backward-kill-partword
bindkey '^e' end-of-line
bindkey '^a' beginning-of-line
bindkey '^[.' insert-last-word
bindkey '[1;5D' vi-backward-word
bindkey '[1;5C' vi-forward-word

# ability to edit long commands in vi
#https://nuclearsquid.com/writings/edit-long-commands/
autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

# Environment
export EDITOR="$(which vim)"
export VISUAL="$(which vim)"

#aliases
alias la="ls -laG"
alias ls='ls -G'
alias dfh="df -h"
alias gd="git diff --color"
alias gst="git status"

zshfzfbindings=()
zshfzfbindings+=("/usr/share/fzf/shell/key-bindings.zsh")
zshfzfbindings+=("/usr/local/share/examples/fzf/shell/key-bindings.zsh")
zshfzfbindings+=("/usr/share/doc/fzf/examples/key-bindings.zsh")
zshfzfbindings+=("/opt/homebrew/opt/fzf/shell/key-bindings.zsh")
for bindings in "${zshfzfbindings[@]}" ; do
   test -f "$bindings" && source "$bindings" && break
done

export PATH="$HOME/.local/bin:$PATH"

#local stuff
[[ -f ~/.zshrc.local ]] && source ~/.zshrc.local

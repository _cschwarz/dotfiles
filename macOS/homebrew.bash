#!/usr/bin/env bash

set -euo pipefail

brew install tmux vim

brew install iterm2
# restore config from dotfiles repo

brew tap homebrew/cask-fonts
brew install font-iosevka

brew install iterm

brew install alt-tab
# restore config from dotfiles repo

brew install karabiner-elements
# restore config from dotfiles repo

brew install utm

brew install google-chrome

brew install gh

brew install monitorcontrol

brew install bettertouchtool

brew install linearmouse
# restore config from dotfiles repo

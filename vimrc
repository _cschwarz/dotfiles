set nocompatible

filetype plugin indent on

augroup vimrcEx
au!
" For all text files set 'textwidth' to 78 characters.
autocmd FileType text setlocal textwidth=78
" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
" Also don't do it when the mark is in the first line, that is the default
" position when opening a file.
autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif
augroup END "vimrcEx

""" EDITING

" Always use UTF8
set encoding=utf-8
" Set up visible whitespace by default (stolen from tkolb)
set list
"set listchars=tab:▶–,trail:◀,extends:>,precedes:< ",eol:↵
set listchars=tab:\ \ ,trail:◀,extends:>,precedes:<
"set listchars=trail:◀,extends:>,precedes:< ",eol:↵
"highlight NonText guifg=#808080 ctermfg=DarkGray
"highlight SpecialKey guifg=#808080 ctermfg=DarkGray
set showbreak=~>

""" Smartcase Search
set ignorecase
set smartcase

""" UI
" but accept a modeline if given
set modeline
" Split in a way that feels more natural
set splitbelow
set splitright
" allow backspacing over everything in insert mode
set backspace=indent,eol,start
" misc
set backup
set history=10000
set ruler
set showcmd
set incsearch
if has('mouse')
  set mouse=a
endif
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif
" GVIM
if has('gui_running')
    set guioptions=m
endif

" basic theming
autocmd ColorScheme * highlight VertSplit cterm=NONE ctermbg=NONE ctermfg=NONE gui=NONE guifg=NONE guibg=NONE
colorscheme default
set termguicolors
set background=light

""" FORMATTING

""" Preferred indentation: 4 spaces
autocmd FileType * set tabstop=4
autocmd FileType * set shiftwidth=4
autocmd FileType * set expandtab

autocmd FileType make set noexpandtab
autocmd FileType go set noexpandtab

autocmd FileType {tex,yaml} set tabstop=2 shiftwidth=2
" autocmd FileType tex set shiftwidth=2

""" SHORTCUTS
let mapleader = ","
" Yank current path + lineno to system clipboard
" creds: https://stackoverflow.com/questions/17498144/yank-file-path-with-line-no-from-vim-to-system-clipboard
nmap <leader>y :let @+=expand("%") . ':' . line(".")<CR>
" Copy selection to system clipboard
vmap <C-Y> "+y
" Toggle relative line numbers
map <C-L> :set number! relativenumber! <CR>

""" Install vim-plug
command! CSPlugDL !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

if !empty(glob('~/.vim/autoload/plug.vim'))
call plug#begin('~/.vim/plugged') " plug plugins go into ~/.vim/plugged
Plug 'rafi/awesome-vim-colorschemes'
Plug 'mitsuhiko/fruity-vim-colorscheme'
Plug 'junegunn/fzf.vim'
Plug 'vim-scripts/Fruidle'
Plug 'sjl/gundo.vim'
Plug 'chase/vim-ansible-yaml'
Plug 'shiracamus/vim-syntax-x86-objdump-d'
Plug 'justinmk/vim-syntax-extra'
Plug 'jamessan/vim-gnupg'
Plug 'scrooloose/nerdtree'
Plug 'airblade/vim-gitgutter'
Plug 'fatih/vim-go'
Plug 'tpope/vim-eunuch'
Plug 'mileszs/ack.vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'zchee/deoplete-go', { 'do': 'make'}
if has('nvim')
    Plug 'sebastianmarkow/deoplete-rust'
endif

Plug 'tpope/vim-sensible' " try this out, they say 'sensible defaults everyone can agree on'

Plug 'phongvcao/nerdtree-yank-plugin'

" Rust Language server: https://rls.booyaa.wtf/
Plug 'rust-lang/rust.vim'
Plug 'prabirshrestha/async.vim', { 'on': [] }
Plug 'prabirshrestha/vim-lsp', { 'on': [] }
Plug 'prabirshrestha/asyncomplete.vim', { 'on': [] }
Plug 'prabirshrestha/asyncomplete-lsp.vim', { 'on': [] }

Plug 'bkad/camelcasemotion'

Plug 'woelke/vim-nerdtree_plugin_open'

Plug 'takac/vim-hardtime'

Plug 'tpope/vim-commentary'

Plug 'easymotion/vim-easymotion'
Plug 'haya14busa/incsearch.vim'

Plug 'neomake/neomake'

Plug 'skywind3000/asyncrun.vim'

Plug 'w0rp/ale'

call plug#end()
endif

" deoplete stuff
let g:deoplete#enable_at_startup = 0
let g:deoplete#sources#rust#rust_source_path='/home/cs/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src'
let g:deoplete#sources#rust#racer_binary='/home/cs/.cargo/bin/racer'

" camelcasemotion-provided mappings (Space + (v|b|e))
map <silent>  w <Plug>CamelCaseMotion_w
map <silent>  b <Plug>CamelCaseMotion_b
map <silent>  e <Plug>CamelCaseMotion_e

let g:nerdtree_plugin_open_cmd = 'xdg-open'


" NERDTree
let g:NERDTreeRespectWildignore = 1

" vim-go
let g:go_auto_sameids = 1
let g:go_fmt_autosave = 0
au FileType go nmap <leader>i :GoInfo<CR>
au FileType go nmap <leader>gm :GoImports
au FileType go nmap <leader>b :GoBuild<CR>
au FileType go nmap <leader>t :GoTest<CR>

" rust
au Filetype rust nmap <leader>b :Neomake<CR>
au Filetype rust call deoplete#enable()
let g:rustfmt_autosave = 1

" yaml
au Filetype yaml set cursorcolumn

""" fzf.vim shortcuts
nmap <leader>ff :Files<CR>
nmap <leader>fg :GFiles<CR>
nmap <leader>fb :Buffers<CR>
nmap <leader>fl :Lines<CR>
"let g:fzf_buffers_jump = 1

""" vim-gnupg plugin
let g:GPGExecutable = "gpg2"

""" rust stuff: https://rls.booyaa.wtf/
if executable('rls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'rls',
        \ 'cmd': {server_info->['rustup', 'run', 'nightly', 'rls']},
        \ 'whitelist': ['rust'],
        \ })
endif
let g:asyncomplete_smart_completion = 1 " fuzzy completion

""" color schemes
function! HasColorscheme(name) abort
    let pat = 'colors/'.a:name.'.vim'
    return !empty(globpath(&rtp, pat))
endfunction

if HasColorscheme("molokai")  """ cater for the case where no colorschem is installed yet
let g:CSColorSchemes = [ "molokai", "fruidle" ]
function! CSCycleColorSchemes()
    execute "colorscheme " . g:CSColorSchemes[0]
    let g:CSColorSchemes = g:CSColorSchemes[1:] + [g:CSColorSchemes[0]]
endfunction
call CSCycleColorSchemes()
endif

let g:ackprg = 'ag --vimgrep'

""" GUI font resizing
if has('gui_running')
    nnoremap <leader><leader>n :call CSFontSizeDecrease()<cr>
    nnoremap <leader><leader>m :call CSFontSizeIncrease()<cr>
    let g:cs_font_size = 10
    function! CSFontSizeIncrease()
        let g:cs_font_size = g:cs_font_size + 1
        call CSFontSizeUpdate()
    endfunction
    function! CSFontSizeDecrease()
        let g:cs_font_size = g:cs_font_size - 1
        call CSFontSizeUpdate()
    endfunction
    function! CSFontSizeUpdate()
        let &guifont="Monospace " . g:cs_font_size
    endfunction
endif

""" incsearch
if exists(":IncSearch")
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
endif

""" pasting into a terminal buffer
if has('nvim')
    """ paste into terminal
    """ http://vimcasts.org/episodes/neovim-terminal-paste/
    tnoremap <expr> <A-r> '<C-\><C-N>"'.nr2char(getchar()).'pi'
endif

nmap <leader><leader>t :terminal<CR>

""" ale
let g:ale_enabled = 0
let g:ale_linters_explicit = 1
let g:ale_linters = {
    \   'go': ['staticcheck', 'golangci-lint' ],
    \}
let g:ale_go_golangci_lint_package = 1

let g:neomake_golangci_maker = {
            \ 'exe': 'golangci-lint',
            \ 'args': ['run', '--out-format=line-number', '--print-issued-lines=false', './...'],
            \ 'errorformat': '%f:%l:%c: %m',
            \ }

"""fzf-vim dependencies
"""" debian
let s:fzf_vim_paths = []
call add(s:fzf_vim_paths, "/usr/share/doc/fzf/examples/fzf.vim")
for p in s:fzf_vim_paths
    if filereadable(p)
        execute 'source' p
        break
    endif
endfor

""" Local config
if filereadable(glob("~/.vimrc.local")) 
    source ~/.vimrc.local
endif



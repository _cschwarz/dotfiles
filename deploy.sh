#!/usr/bin/env bash
set -euo pipefail

cd $( dirname "${BASH_SOURCE[0]}")

if [ "$(readlink -f ".")" != "$(readlink -f "$HOME/dotfiles")" ];then
	echo "expecting to be checked out into subdirectory dotfiles within the user's HOME" 1>&2
	exit 128
fi

cd ..

install -d ~/.config/git
ln -sf dotfiles/gitignore 	.config/git/ignore
ln -sf dotfiles/gitconfig 	.config/git/config
ln -sf dotfiles/vimrc 		.vimrc
ln -sf dotfiles/tmux.conf 	.tmux.conf
ln -sf dotfiles/zshrc 		.zshrc
